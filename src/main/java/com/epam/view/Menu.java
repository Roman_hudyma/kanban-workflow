package com.epam.view;

import com.epam.model.Board;
import com.epam.model.Developer;
import com.epam.model.Task;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {

  protected static Scanner input = new Scanner(System.in);
  protected static Board board = new Board();
  protected static Task currentTask;
  protected static Developer currentDeveloper;
  private static Logger log = LogManager.getLogger(Menu.class);
  private Map<String, Printable> methodsMenu;
  private Map<String, String> taskMenu;

  public Menu() {
    taskMenu = new LinkedHashMap<>();
    taskMenu.put("1", "1 - Add task");
    taskMenu.put("2", "2 - Choose task");
    taskMenu.put("Q", "Q - Exit\n");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::method1);
    methodsMenu.put("2", this::method2);
  }

  private void displayTable() {
    int count = 1;
    for (Task task : board.getTasks()) {
      task.setId(count++);
      log.info(String.format("%4d%25s%25s%25s", task.getId(), task.getName(), task.getState(),
          (task.getDeveloper() == null) ? "--------" : task.getDeveloper()));
    }
  }

  private void outputMenu() {
    log.trace("\nMENU:");
    for (String str : taskMenu.values()) {
      log.info(str);
    }
  }

  private void method1() {
    log.info("Enter Task description");
    Task task = new Task();
    task.setName(input.nextLine());
    board.addTask(task);
  }

  private void method2() {
    log.info("Choose Task number");
    int taskNumber = Integer.parseInt(input.nextLine().trim());
    if ((taskNumber <= board.getTasks().size()) && (taskNumber > 0)) {
      currentTask = board.getTasks().get(taskNumber - 1);
      log.info(currentTask);
      new ActionsMenu().showActions();
    } else {
      log.warn("Wrong input");
    }
  }

  public void show() {
    String keyMenu;
    do {
      log.trace(String.format("%4s%25s%25s%25s", "Id", "Task", "State", "Developer"));
      displayTable();
      outputMenu();
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
