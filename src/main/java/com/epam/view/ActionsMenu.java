package com.epam.view;

import static com.epam.view.Menu.currentDeveloper;
import static com.epam.view.Menu.currentTask;
import static com.epam.view.Menu.input;

import com.epam.model.Team;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ActionsMenu {

  private static Logger log = LogManager.getLogger(ActionsMenu.class);
  private Map<String, Printable> methodsMenu;
  private Map<String, String> actionsMenu;

  public ActionsMenu() {
    actionsMenu = new LinkedHashMap<>();
    actionsMenu.put("1", "1 - Start to develop task");
    actionsMenu.put("2", "2 - Send task to review");
    actionsMenu.put("3", "3 - Approve task");
    actionsMenu.put("4", "4 - Test task");
    actionsMenu.put("5", "5 - Complete task");
    actionsMenu.put("6", "6 - Delete task");
    actionsMenu.put("7", "7 - Restore task");
    actionsMenu.put("Q", "Q - Back to task menu\n");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::method1);
    methodsMenu.put("2", this::method2);
    methodsMenu.put("3", this::method3);
    methodsMenu.put("4", this::method4);
    methodsMenu.put("5", this::method5);
    methodsMenu.put("6", this::method6);
    methodsMenu.put("7", this::method7);
  }

  private void method1() {
    displayDevelopers();
    chooseDeveloper();
    currentTask.startToDevelopTask(currentDeveloper);
  }

  private void method2() {
    displayDevelopers();
    chooseDeveloper();
    currentTask.sendToReviewTask(currentDeveloper);
  }

  private void method3() {
    displayDevelopers();
    chooseDeveloper();
    currentTask.approveTask(currentDeveloper);
  }

  private void method4() {
    displayDevelopers();
    chooseDeveloper();
    currentTask.testTask(currentDeveloper);
  }

  private void method5() {
    currentTask.completeTask();
  }

  private void method6() {
    currentTask.deleteTask();
  }

  private void method7() {
    displayDevelopers();
    chooseDeveloper();
    currentTask.restoreTask(currentDeveloper);
  }

  private void chooseDeveloper() {
    int developerNumber = Integer.parseInt(input.nextLine().trim());
    if ((developerNumber <= Team.getDevelopers().size()) && (developerNumber > 0)) {
      currentDeveloper = Team.getDevelopers().get(developerNumber - 1);
      log.info(currentDeveloper);
    } else {
      log.warn("Wrong input");
    }
  }

  private void displayDevelopers() {
    log.info("Please, choose developer's number");
    for (int i = 0; i < Team.getDevelopers().size(); i++) {
      log.info((i + 1) + ". " + Team.getDevelopers().get(i).toString());
    }
  }

  private void outputMenu1() {
    log.info("\nMENU:");
    for (String str : actionsMenu.values()) {
      log.info(str);
    }
  }

  public void showActions() {
    String keyMenu;
    do {
      outputMenu1();
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}

