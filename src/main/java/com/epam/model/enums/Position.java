package com.epam.model.enums;

public enum Position {
  DEVELOPER, TEAM_LEAD, QA
}
