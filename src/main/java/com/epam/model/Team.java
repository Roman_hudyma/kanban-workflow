package com.epam.model;

import com.epam.model.enums.Position;
import java.util.ArrayList;
import java.util.List;

public class Team {

  private static List<Developer> developers = new ArrayList<>();

  static {
    developers.add(new Developer("Sergey Brin", Position.DEVELOPER));
    developers.add(new Developer("Bill Gates", Position.DEVELOPER));
    developers.add(new Developer("Bjarne Stroustrup", Position.DEVELOPER));
    developers.add(new Developer("James Gosling", Position.TEAM_LEAD));
    developers.add(new Developer("Mike Sheridan", Position.DEVELOPER));
    developers.add(new Developer("James Bach", Position.QA));
  }

  public Team() {
  }

  public static List<Developer> getDevelopers() {
    return developers;
  }
}
