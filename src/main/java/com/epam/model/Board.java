package com.epam.model;

import java.util.ArrayList;
import java.util.List;

public class Board {

  private List<Task> tasks = new ArrayList<>();

  public Board() {
  }

  public List<Task> getTasks() {
    return tasks;
  }

  public void setTasks(List<Task> tasks) {
    this.tasks = tasks;
  }

  public void addTask(Task task) {
    tasks.add(task);
  }

  public void removeTask(Task task) {
    tasks.remove(task);
  }
}
