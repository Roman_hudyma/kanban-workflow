package com.epam.model;

import com.epam.model.state.State;
import com.epam.model.state.impl.ToDo;

public class Task {

  private int id;
  private String name;
  private State state;
  private Developer developer;

  public Task() {
    state = new ToDo();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void startToDevelopTask(Developer developer) {
    state.startToDevelop(this, developer);
  }

  public void sendToReviewTask(Developer developer) {
    state.sendToReview(this, developer);
  }

  public void deleteTask() {
    state.delete(this);
  }

  public void restoreTask(Developer developer) {
    state.restore(this, developer);
  }

  public void approveTask(Developer developer) {
    state.approve(this, developer);
  }

  public void testTask(Developer developer) {
    state.test(this, developer);
  }

  public void completeTask() {
    state.complete(this);
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  public Developer getDeveloper() {
    return developer;
  }

  public void setDeveloper(Developer developer) {
    this.developer = developer;
  }

  @Override
  public String toString() {
    return "Task " + name;
  }
}