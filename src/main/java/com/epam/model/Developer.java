package com.epam.model;

import com.epam.model.enums.Position;
import java.util.List;

public class Developer {

  private String name;
  private Position position;
  private List<Task> tasks;

  public Developer(String name, Position position) {
    this.name = name;
    this.position = position;
  }

  public String getName() {
    return name;
  }

  public Position getPosition() {
    return position;
  }

  public List<Task> getTasks() {
    return tasks;
  }

  public void setPosition(Position position) {
    this.position = position;
  }

  @Override
  public String toString() {
    return position + " " + name;
  }
}
