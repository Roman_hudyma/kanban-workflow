package com.epam.model.state.impl;

import com.epam.model.Developer;
import com.epam.model.Task;
import com.epam.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Development implements State {

  private static Logger log = LogManager.getLogger(ToDo.class);
  protected Task task;

  @Override
  public void sendToReview(Task task, Developer developer) {
    State state = new CodeReview();
    state.setTask(this.task);
    if (developer != null) {
      state.setDeveloper(developer);
    }

    task.setState(state);
    log.info(String.format(" %s moved to code review state", task));
  }

  @Override
  public void delete(Task task) {
    Deleted state = new Deleted();
    state.setStateBeforeDeleting(task.getState());
    state.setTask(task);
    task.setState(state);
    task.setDeveloper(null);
    log.info(String.format(" %s moved to deleted state", task));
  }

  @Override
  public void setDeveloper(Developer developer) {
    this.task.setDeveloper(developer);
    log.info(String.format("Added %s to %s", developer, task));
  }

  @Override
  public void setTask(Task task) {
    this.task = task;
  }

  @Override
  public String toString() {
    return "Development";
  }
}
