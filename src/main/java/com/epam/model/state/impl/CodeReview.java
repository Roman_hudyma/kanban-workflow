package com.epam.model.state.impl;

import com.epam.model.Developer;
import com.epam.model.Task;
import com.epam.model.enums.Position;
import com.epam.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CodeReview implements State {

  private static Logger log = LogManager.getLogger(ToDo.class);
  private Task task;
  private int approvesCount;

  @Override
  public void startToDevelop(Task task, Developer developer) {
    State state = new Development();
    state.setTask(task);
    if (developer != null) {
      state.setDeveloper(developer);
    }
    task.setState(state);
    log.info(String.format(" %s moved to development state", task));
  }

  @Override
  public void delete(Task task) {
    Deleted state = new Deleted();
    state.setStateBeforeDeleting(task.getState());
    state.setTask(task);
    task.setState(state);
    task.setDeveloper(null);
    log.info(String.format(" %s moved to deleted state", task));
  }

  @Override
  public void approve(Task task, Developer developer) {
    Position position = developer.getPosition();
    switch (position) {
      case DEVELOPER:
        approvesCount++;
        break;
      case TEAM_LEAD:
        approvesCount += 3;
        break;
      default:
    }
    log.info(String.format(" %s approved. Current approves equals %d", task, approvesCount));
  }

  @Override
  public void test(Task task, Developer developer) {
    if ((approvesCount >= 3) && (developer.getPosition() == Position.QA)) {
      State state = new Testing();
      state.setTask(task);
      if (developer != null) {
        state.setDeveloper(developer);
      }
      task.setState(state);
      log.info(String.format("Moved %s to testing state", task));
    } else {
      log.warn(String.format(
          "Can't move to testing state because %s have less then 3 approves "
              + "\n or employee %s is not QA", task, developer));
    }
  }

  @Override
  public void setDeveloper(Developer developer) {
    this.task.setDeveloper(developer);
    log.info(String.format("Added developer %s to %s", developer, task));
  }

  @Override
  public void setTask(Task task) {
    this.task = task;
  }

  @Override
  public String toString() {
    return "CodeReview";
  }
}
