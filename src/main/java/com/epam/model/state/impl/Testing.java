package com.epam.model.state.impl;

import com.epam.model.Developer;
import com.epam.model.Task;
import com.epam.model.enums.Position;
import com.epam.model.state.State;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Testing implements State {

  private static Logger log = LogManager.getLogger(ToDo.class);
  private Task task;

  @Override
  public void startToDevelop(Task task, Developer developer) {
    State state = new Development();
    state.setTask(task);
    if (developer != null) {
      state.setDeveloper(developer);
    }
    task.setState(state);
  }

  @Override
  public void delete(Task task) {
    Deleted state = new Deleted();
    state.setStateBeforeDeleting(task.getState());
    state.setTask(task);
    task.setState(state);
    task.setDeveloper(null);
    log.info(String.format(" %s moved to deleted state", task));
  }

  @Override
  public void complete(Task task) {
    task.setDeveloper(null);
    State state = new Done();
    state.setTask(task);
    task.setState(state);
  }

  @Override
  public void setDeveloper(Developer developer) {
    if (developer.getPosition() == Position.QA) {
      this.task.setDeveloper(developer);
    } else {
      log.warn("Position must be a QA");
    }
  }

  @Override
  public void setTask(Task task) {
    this.task = task;
  }

  @Override
  public String toString() {
    return "Testing";
  }
}
