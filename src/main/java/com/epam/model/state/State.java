package com.epam.model.state;

import com.epam.model.Developer;
import com.epam.model.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface State {

  Logger log = LogManager.getLogger(State.class);

  default void startToDevelop(Task task, Developer developer) {
    log.warn("You can't do this action in current task state");
  }

  default void sendToReview(Task task, Developer developer) {
    log.warn("You can't do this action in current task state");
  }

  default void delete(Task task) {
    log.info("You can't do this action in current task state");
  }

  default void restore(Task task, Developer developer) {
    log.warn("You can't do this action in current task state");
  }

  default void approve(Task task, Developer developer) {
    log.warn("You can't do this action in current task state");
  }

  default void test(Task task, Developer developer) {
    log.warn("You can't do this action in current task state");
  }

  default void complete(Task task) {
    log.warn("You can't do this action in current task state");
  }

  default void setDeveloper(Developer developer) {
    log.warn("You can't add developer to task in this state");
  }

  default void setTask(Task task) {
    log.warn("You can't add/change developer in this state");
  }
}
